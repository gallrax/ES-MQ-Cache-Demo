package com.test;

import com.cx.entity.Depart;
import com.cx.entity.User;
import com.cx.repository.es.UserESRepository;
import com.cx.repository.jpa.DepartRepository;
import com.cx.repository.jpa.UserRepository;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.WildcardQueryBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @Description:
 * @Date: Created on 2018/3/5
 * @Version: 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserTest {

    @Autowired
    UserRepository userRepository;
    @Autowired
    UserESRepository userESRepository;
    @Autowired
    DepartRepository departRepository;
    @Autowired
    RedisTemplate redisTemplate;

    @Test
    public void test01() {
        for (int i = 1; i < 100; i++) {
            String uuid = UUID.randomUUID().toString();
            User user = new User(uuid, "tom" + i, 1, "1381111111" + i, new Date());
            userRepository.save(user);
            userESRepository.save(user);
        }
    }

    @Test
    public void test02() {
        Page<User> users = userRepository.findAll(new PageRequest(2, 3));
        System.out.println(users.getContent());
        Page<User> temp = userRepository.findAll(new PageRequest(2, 3));
    }

    /**
     * 测试spring cache
     */
    @Test
    public void test03() {
        User tom1 = userRepository.findByName("tom1");
        User tom2 = userRepository.findByName("tom2");
        System.out.println(" 第一次tom1 : " + tom1);
        User tempTom = userRepository.findByName("tom1");
        System.out.println(" 第二次tom1 : " + tempTom);
        tom1.setSex(2);
        userRepository.save(tom1);
        System.out.println(" save user end ");
        User user = userRepository.findByName("tom1");
        System.out.println(" find from cache ? " + user.getSex());
        /*userRepository.delete(tom1.getId());
        //证明是否为清空所有
        User tempTom1 = userRepository.findByName("tom1");
        User tempTom2 = userRepository.findByName("tom2");
        System.out.println(" tempTom1 : " + tempTom1);
        System.out.println(" tempTom2 : " + tempTom2);*/
    }

    /**
     * 测试spring cache name定义是否有作用(制定name之后，key的规则一样。实际结果证明此种方式不靠谱，当id相同时则会抛出类型不匹配异常)
     */
    @Test
    public void test04() {
//        User user = userRepository.findById("574a4f31-035a-4272-9c62-2f73d1b87fa8");
//        System.out.println(user);
//        User tempUser = userRepository.findById("574a4f31-035a-4272-9c62-2f73d1b87fa8");
//        System.out.println(tempUser);
//        Depart depart1 = departRepository.findById("1");
//        System.out.println(depart1);
        Depart depart2 = departRepository.findById("574a4f31-035a-4272-9c62-2f73d1b87fa8");
        System.out.println(depart2);
        Depart tempDepart = departRepository.findById("574a4f31-035a-4272-9c62-2f73d1b87fa8");
        System.out.println(tempDepart);
    }

    @Test
    public void test05() {
        User tom1 = userRepository.findByName("tom1");
        System.out.println("第一次查询 ： " + tom1);
        User tempTom1 = userRepository.findByName("tom1");
        System.out.println("第二次查询 ： " + tempTom1);
        tom1.setSex(2);
        User save = userRepository.save(tom1);
        System.out.println("修改之后 ： " + save.getSex());
        User updateTom1 = userRepository.findByName("tom1");
        System.out.println(" cache ? : " + updateTom1.getSex());
    }

    @Test
    public void test06() {
        User user1 = userRepository.findById("574a4f31-035a-4272-9c62-2f73d1b87fa8");
        System.out.println(" 第一次查询 : " + user1);
        User user2 = userRepository.findById("574a4f31-035a-4272-9c62-2f73d1b87fa8");
        System.out.println(" 第二次查询 : " + user2);
        user1.setSex(user1.getSex() + 1);
        User save = userRepository.save(user1);
        System.out.println(" 修改之后 : " + save);
        User user3 = userRepository.findById("574a4f31-035a-4272-9c62-2f73d1b87fa8");
        System.out.println(" 测试缓存 ： " + user3);
        User user4 = userRepository.findById("574a4f31-035a-4272-9c62-2f73d1b87fa8");
        System.out.println(" 测试缓存2 ： " + user4);
    }

    @Test
    public void test07() {
        Page<User> page1 = userESRepository.findByName("tom8", new PageRequest(0, 10));
        int totalPages1 = page1.getTotalPages();
        List<User> list1 = page1.getContent();
        System.out.println(" page1 total Pages : " + totalPages1);
        System.out.println(" page1 list : " + list1);
        Page<User> page2 = userESRepository.findByNameLikeOrPhoneLike("2", "2", new PageRequest(0, 10));
        int totalPages2 = page2.getTotalPages();
        List<User> list2 = page2.getContent();
        System.out.println(" page2 total Pages : " + totalPages2);
        System.out.println(" page2 list : " + list2);
    }

    @Test
    public void test08() {
        Iterable<User> userIterable = userESRepository.findAll();
        for (User user : userIterable) {
            System.out.println(user);//tom28
        }
    }

    @Test
    public void test09() {
        User user = userESRepository.findOne("7343cdcb-5cdf-47b7-9170-9f73c91757f5");
        System.out.println(user);
    }

    @Test
    public void test10() {
        List<User> userList = userESRepository.findByNameLike("tom2");
        for (User user : userList) {
            System.out.println(user);
        }
    }

    @Test
    public void test11() {
        List<User> users = userESRepository.findByName("tom8");
        for (User user : users) {
            System.out.println(" user : " + user);
        }
    }

    @Test
    public void test12() {
        String query = "tom";
//        QueryStringQueryBuilder builder = new QueryStringQueryBuilder(query);
//        FuzzyQueryBuilder fuzzyQueryBuilder = QueryBuilders.fuzzyQuery("name", "tom8");
        WildcardQueryBuilder wildcardQueryBuilder = QueryBuilders.wildcardQuery("name", "*2*");
        Iterable<User> search = userESRepository.search(wildcardQueryBuilder);
        for (User user : search) {
            System.out.println(user);
        }
    }

    @Test
    public void testTemp() {
        Runnable runnable = () -> System.out.println("-");
        new Thread(runnable).start();
        KeyGenerator keyGenerator = (o, m, params) -> params[0];
    }

}
