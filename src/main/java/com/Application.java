package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @Description:
 * @Date: Created on 2018/3/5
 * @Version: 1.0
 */
@SpringBootApplication
@EnableAutoConfiguration
@EnableJpaRepositories(basePackages = "com.cx.repository.jpa")
@EnableElasticsearchRepositories(basePackages = "com.cx.repository.es")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
