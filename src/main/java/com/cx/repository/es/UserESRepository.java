package com.cx.repository.es;

import com.cx.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

/**
 * @Description:
 * @Date: Created on 2018/3/7
 * @Version: 1.0
 */
public interface UserESRepository extends ElasticsearchRepository<User, String> {

    @Query("{\"bool\" : {\"must\" : {\"term\" : {\"name\" : \"?0\"}}}}")
    List<User> findByName(String name);

    Page<User> findByName(String name, Pageable pageable);

    List<User> findByNameLike(String name);

    @Query("{\"bool\" : {\"should\" : [ {\"field\" : {\"name\" :  {\"query\" : \"?0\",\"analyze_wildcard\" : true}}}, {\"field\" : {\"price\" :  {\"query\" : \"?1\",\"analyze_wildcard\" : true}}} ]}}")
    Page<User> findByNameLikeOrPhoneLike(String name, String phone, Pageable pageable);


}
