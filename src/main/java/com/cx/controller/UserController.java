package com.cx.controller;

import com.cx.annotation.SysLog;
import com.cx.entity.User;
import com.cx.repository.es.UserESRepository;
import com.cx.repository.jpa.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.UUID;

/**
 * @Description:
 * @Date: Created on 2018/3/5
 * @Version: 1.0
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserESRepository userESRepository;

    @SysLog
    @RequestMapping("/init")
    public String init() {
        try {
            for (int i = 0; i < 100; i++) {
                User user = new User(UUID.randomUUID().toString(), "tom" + i, 1, "1381111111" + i, new Date());
                userRepository.save(user);
                userESRepository.save(user);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
        return "success";
    }

    @SysLog
    @RequestMapping("/findAll/{currentPage}/{size}")
    public Page<User> findAll(@PathVariable(value = "currentPage") Integer currentPage,
                              @PathVariable(value = "size") Integer size) {
        PageRequest pageRequest = new PageRequest(currentPage, size);
        Page<User> page = userRepository.findAll(pageRequest);
        return page;
    }

    @SysLog
    @RequestMapping("/find/{id}")
    public User findById(@PathVariable("id") String id) {
        User user = userRepository.findById(id);
        return user;
    }

    @SysLog
    @RequestMapping("/search")
    public Page<User> search() {

        return null;
    }

}
